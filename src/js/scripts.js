$(document).ready(function(){

    // Bottom carousel

    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        lazyLoad: 'ondemand',
        fade: true,
        focusOnSelect: true,
        asNavFor: '.slider-nav',
        responsive: [
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '0px'
          }
        }]
   });

    // Bottom Slider Navigation

    $('.slider-nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        arrows: true,
        focusOnSelect: true,
        infinite: true,
        centerMode: true,
        adaptiveHeight: true,
        responsive: [
        {
          breakpoint: 768,
          settings: {
            centerMode: true,
            centerPadding: '0px',
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }]
    });

   // Testimonial Slider
    $(".testimonialSlider").slick({
        dots: true,
        vertical: false,
        centerMode: true,
        slidesToShow: 1,
        fade: true,
        slidesToScroll: 1,
        arrows: true,
        responsive: [
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '0px',
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }]
    });


    var prevScrollpos = window.pageYOffset;
        window.onscroll = function() {
            var currentScrollPos = window.pageYOffset;
            if (prevScrollpos > currentScrollPos) {
            document.getElementById("navbar").style.top = "0";
        } else {
            document.getElementById("navbar").style.top = "-180px";
        }
            prevScrollpos = currentScrollPos;
    }

 });