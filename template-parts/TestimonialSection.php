<section class="TestimonialBlock BgColorBlue">
	<div class="container">
		<h2>From Great Our Clients</h2>
		<div class="TestimonialSlider">
			<div class="testimonialSlider slider">
				<div class="SlidesBlock">
					<img src="assets/img/img/temp-img/testimonial.png">
					<h4>Jon Smith CEO</h4>
					<span>Strauss Sport</span>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quaerat quod, libero tenetur reiciendis suscipit temporibus ipsa, voluptatibus nemo enim, officiis illum cumque iusto quis magni.</p>
				</div>
				<div class="SlidesBlock">
					<img src="assets/img/img/temp-img/testimonial.png">
					<h4>Gym Junkie</h4>
					<span>Strauss Sport</span>
					<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release.</p>
				</div>
				<div class="SlidesBlock">
					<img src="assets/img/img/temp-img/testimonial.png">
					<h4>Jon Smith CEO</h4>
					<span>Strauss Sport</span>
					<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
				</div>
				<div class="SlidesBlock">
					<img src="assets/img/img/temp-img/testimonial.png">
					<h4>Gym Junkie</h4>
					<span>Strauss Sport</span>
					<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even.</p>
				</div>
			</div>
		</div>
	</div>
</section>