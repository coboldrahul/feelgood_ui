<section class="LeftContentRightImageBlock BgColorBlue">
	<div class="GridWrapper">
		<div class="Grid DesktopOnly">
			<div class="LeftContent">
				<h2>Motion Beds</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni ratione numquam fugiat vel corporis fuga sint qui quod, ab odit laudantium fugit quasi incidunt nam.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni ratione numquam fugiat vel corporis fuga sint qui quod, ab odit laudantium fugit quasi incidunt nam.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni ratione numquam fugiat vel corporis fuga sint qui quod, ab odit laudantium fugit quasi incidunt nam.</p>
				<p class="ButtonElement"><a href="" class="Button"><span>Discover</span></a></p>
			</div>
		</div>

		<div class="Grid">
				<img src="assets/img/img/temp-img/motion-bed.png">
		</div>

		<div class="Grid MobileOnly">
			<div class="LeftContent">
				<h2>Motion Beds</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni ratione numquam fugiat vel corporis fuga sint qui quod, ab odit laudantium fugit quasi incidunt nam.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni ratione numquam fugiat vel corporis fuga sint qui quod, ab odit laudantium fugit quasi incidunt nam.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni ratione numquam fugiat vel corporis fuga sint qui quod, ab odit laudantium fugit quasi incidunt nam.</p>
				<p class="ButtonElement"><a href="" class="Button"><span>Discover</span></a></p>
			</div>
		</div>

	</div>
</section>
