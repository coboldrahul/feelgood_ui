<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Feel Good</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width,initial-scale=1">

	<link href="assets/css/vendor.min.css" rel="stylesheet">
	<link href="assets/css/styles.min.css" rel="stylesheet">
	<link rel="shortcut icon" type="image/png" href="assets/img/img/temp-img/footerlogo.png"/>
</head>

<body>
	<header id="navbar">
		<div class="container">
			<div class="row">
				<div class="col-12 col-md-12">
					<a href=""><img class="Logo" src="assets/img/img/temp-img/logo.png"></a>
				</div>				
			</div>
		</div>
		<div class="HeaderMenuBox">
			<div class="container">
				<div class="row">
					<div class="col-12 col-md-12">
						<div class="MenuHeader">
							<nav>
								<div class="DesktopMenu">
									<ul>
										<li>
											<a href="" class="current-menu-item">Home</a>
										</li>
										<li class="menu-item menu-item-has-children">
											<a href="">Sleep Systems</a>
											<ul class="sub-menu">
												<li><a href="">Adjustable Beds</a></li>
												<li><a href="">Mattress</a></li>
												<li><a href="">Pillows</a></li>
											</ul>
										</li>
										<li>
											<a href="">Quality of Sleep</a>
										</li>
										<li class="menu-item menu-item-has-children">
											<a href="">Why Choose Feel Good</a>
											<ul class="sub-menu">
												<li><a href="">USP</a></li>
												<li><a href="">Testimonials</a></li>
												<li><a href="">Sitemap</a></li>
											</ul>
										</li>
										<li class="menu-item menu-item-has-children">
											<a href="">About Us</a>
											<ul class="sub-menu">
												<li><a href="">Profile</a></li>
												<li><a href="">Franchise & Partnership</a></li>
												<li><a href="">Dealex</a></li>
											</ul>
										</li>
										<li><a href="">Contact Us</a></li>
									</ul>
								</div>

							
							    <button class="grt-mobile-button">
							        <div class="line1"></div>
							        <div class="line2"></div>
							        <div class="line3"></div>
							    </button>
							    <ul class="grt-menu">
							        <li class="active"><a href="">Home</a></li>
							        <li class="grt-dropdown">
										<a href="">Sleep Systems</a>
										<ul class="grt-dropdown-list">
											<li><a href="">Adjustable Beds</a></li>
											<li><a href="">Mattress</a></li>
											<li><a href="">Pillows</a></li>
										</ul>
									</li>
							        <li><a href="">Quality of Sleep</a></li>
							        <li class="grt-dropdown">
										<a href="">Why Choose Feel Good</a>
										<ul class="grt-dropdown-list">
											<li><a href="">USP</a></li>
											<li><a href="">Testimonials</a></li>
											<li><a href="">Sitemap</a></li>
										</ul>
									</li>
							        <li class="grt-dropdown">
										<a href="">About Us</a>
										<ul class="grt-dropdown-list">
											<li><a href="">Profile</a></li>
											<li><a href="">Franchise & Partnership</a></li>
											<li><a href="">Dealex</a></li>
										</ul>
									</li>
							        <li><a href="">Contact Us</a></li>
							    </ul>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<main>