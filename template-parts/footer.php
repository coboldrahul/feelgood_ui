
	</main>
	<footer>
		<div class="container">
			<div class="TopFooterMenu">
				<div class="Grid">
					<a href=""><img src="assets/img/img/temp-img/footerlogo.png"></a>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, in!. Lorem ipsum dolor sit amet, consectetur adipisicing elit, in!</p>
				</div>
				<div class="Grid">
					<h4>Sleep Systems</h4>
					<ul class="FooterMenuElement">
						<li><a href="">Adjustable Beds</a></li>
						<li><a href="">Essence</a></li>
						<li><a href="">Pure Slim</a></li>
						<li><a href="">Contour Elite</a></li>
					</ul>
				</div>
				<div class="Grid">
					<h4>Mattress</h4>
					<ul class="FooterMenuElement">
						<li><a href="">Gel infused Memory foam Mattress</a></li>
						<li><a href="">Latex & Gel infused memory foam mattress</a></li>
						<li><a href="">Nature pure Latex Mattress</a></li>
						<li><a href="">Nature Rest Latex Mattress</a></li>
					</ul>
				</div>
				<div class="Grid">
					<h4>Subscribe Newsletter</h4>
					<p class="subscribeTxt">Receive special offers and notifications on our releases.</p>
					<form>
						<input type="email" name="" placeholder="Your email address">
						<input type="submit" name="" value="Subscribe">
					</form>
				</div><!-- 
				<div class="Grid">
					<h4>Get in Touch</h4>
					<ul class="ContactDetails">
						<li><img src="assets/img/img/temp-img/map.png"> Lorem ipsum dolor sit amet.</li>
						<li><a href="tel:+91 11 4659-4898"><img src="assets/img/img/temp-img/phone.png">+91 11 4659-4898</a></li>
						<li><a href="mailto:info@feelgood.com"><img src="assets/img/img/temp-img/email.png">info@feelgood.com</a></li>
					</ul>
				</div> -->
			</div>
			<div class="BottomFooterMenu">
				<div class="Grid">
					<h4>Importance of sleep</h4>
					<ul class="FooterMenuElement">
						<li><a href="">Benefits of Using adjustable bed base</a></li>
						<li><a href="">Benefits of using infused memory foam Mattress</a></li>
						<li><a href="">Benefits of Using latex Mattress</a></li>
					</ul>
					<ul class="SocialIcons">
						<li>
							Follow Us :
						</li>
						<li>
							<a href="" target="_blank" title="Facebbok">
								<svg>
									<use xlink:href="assets/img/img/temp-img/feelgood.svg#Icon-Facebook"></use>
								</svg>
							</a>
						</li>

						<li>
							<a href="" target="_blank" title="Instagram">
								<svg>
									<use xlink:href="assets/img/img/temp-img/feelgood.svg#Icon-Insta"></use>
								</svg>
							</a>
						</li>

						<li>
							<a href="" target="_blank" title="Youtube">
								<svg>
									<use xlink:href="assets/img/img/temp-img/feelgood.svg#icon-linkedin"></use>
								</svg>
							</a>
						</li>

						<li>
							<a href="" target="_blank" title="Twitter">
								<svg>
									<use xlink:href="assets/img/img/temp-img/feelgood.svg#Icon-Twitter"></use>
								</svg>
							</a>
						</li> 						

					</ul>
				</div>
				<div class="Grid">
					<h4>Pillow</h4>
					<ul class="FooterMenuElement">
						<li><a href="">2 Pillows of Gel Memory</a></li>
						<li><a href="">2 Pillows of Geo Memory</a></li>
						<li><a href="">1 Pillows of Mix Foam</a></li>
						<li><a href="">4 Latex Pillow</a></li>
					</ul>
				</div>
				<div class="Grid">
					<h4>Support</h4>
					<ul class="FooterMenuElement">
						<li><a href="">Warranty registration</a></li>
						<li><a href="">Installation</a></li>
						<li><a href="">Downloads</a></li>
						<li><a href="">FAQ's</a></li>
					</ul>
				</div>
				<div class="Grid">
					<h4>Useful Links</h4>
					<ul class="FooterMenuElement">
						<li><a href="">Contact us</a></li>
						<li><a href="">Free Demo</a></li>
						<li><a href="">Blogs</a></li>
					</ul>
				</div>
				<div class="Grid">
					<h4>Quick Links</h4>
					<ul class="FooterMenuElement">
						<li><a href="">Privacy Policy</a></li>
						<li><a href="">Terms of Use</a></li>
						<li><a href="">Sitemap</a></li>
					</ul>
				</div>
				<div class="Grid">
					<h4>Get In Touch</h4>
					<ul class="ContactDetails">
						<li><img src="assets/img/img/temp-img/map.png"> Lorem ipsum dolor sit amet.</li>
						<li><a href="tel:+91 11 4659-4898"><img src="assets/img/img/temp-img/phone.png">+91 11 4659-4898</a></li>
						<li><a href="mailto:info@feelgood.com"><img src="assets/img/img/temp-img/email.png">info@feelgood.com</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="CopyrightBlock">
			<div class="container">
				<div class="row">
					<div class="col-12 col-md-10">
						<p>Copyright <i class="fa fa-copyright" aria-hidden="true"></i> 2019 Feel Good. All rights reserved.</p>
					</div>
					<div class="col-12 col-md-2">
						<p>Credit: <a href="cobold.co" target="_blank">Cobold</a></p>
					</div>
				</div>
			</div>
		</div>
	</footer>
<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/ScrollMagic.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/plugins/debug.addIndicators.min.js"></script>
<script src="assets/js/vendor.min.js"></script>
<script src="assets/js/scripts.min.js"></script>
</body>
</html>