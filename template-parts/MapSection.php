<div class="MapSection MarginTop">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28051.33978438869!2d77.1256923437624!3d28.497084878155018!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d1ef144983aa9%3A0xdd7eaf8d898fe94a!2sGhitorni%2C+New+Delhi%2C+Delhi!5e0!3m2!1sen!2sin!4v1563009774999!5m2!1sen!2sin" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
	<div class="ContactUsDetails">
		<div class="container">
			<div class="FormBox">
				<h3>Get in Touch</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consectetur, vel.</p>
				<form>
					<div class="row">
						<div class="col-12 col-md-6">
							<input type="text" name="" placeholder="First Name">
						</div>
						<div class="col-12 col-md-6">
							<input type="text" name="" placeholder="Last Name">
						</div>
						<div class="col-12 col-md-6">
							<input type="email" name="" placeholder="Email address">
						</div>
						<div class="col-12 col-md-6">
							<input type="text" name="" placeholder="Phone No.">
						</div>
						<div class="col-12 col-md-12">
							<textarea placeholder="Message"></textarea>
						</div>
						<div class="col-12 col-md-12">
							<input type="submit" name="" value="Send">
						</div>
					</div>
				</form>
			</div>
			<div class="InfoBox">
				<h3>Contact Information</h3>
				<ul class="ContactDetails">
					<li><img src="assets/img/img/temp-img/map.png"> Lorem ipsum dolor sit amet.</li>
					<li>Toll Free No</li>
					<li>
						<a href="tel:+91 11 4659-4898"><img src="assets/img/img/temp-img/phone.png">+91 11 4659-4898</a>
					</li>
					<li>
						<a href="tel:+91 11 4659-4898"><img src="assets/img/img/temp-img/phone.png"> +91 11 4659-4898</a>
					</li>
					<li><a href="mailto:info@feelgood.com"><img src="assets/img/img/temp-img/email.png">info@feelgood.com</a></li>
				</ul>
				<ul class="SocialIcons">
						<li>
							Follow Us :
						</li>
						<li>
							<a href="" target="_blank" title="Facebbok">
								<svg>
									<use xlink:href="assets/img/img/temp-img/feelgood.svg#Icon-Facebook"></use>
								</svg>
							</a>
						</li>

						<li>
							<a href="" target="_blank" title="Instagram">
								<svg>
									<use xlink:href="assets/img/img/temp-img/feelgood.svg#Icon-Insta"></use>
								</svg>
							</a>
						</li>

						<li>
							<a href="" target="_blank" title="Youtube">
								<svg>
									<use xlink:href="assets/img/img/temp-img/feelgood.svg#icon-linkedin"></use>
								</svg>
							</a>
						</li>

						<li>
							<a href="" target="_blank" title="Twitter">
								<svg>
									<use xlink:href="assets/img/img/temp-img/feelgood.svg#Icon-Twitter"></use>
								</svg>
							</a>
						</li> 						

					</ul>
			</div>
		</div>
	</div>
</div>