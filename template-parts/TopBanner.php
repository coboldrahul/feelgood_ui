<section class="LeftContentRightImage BgColorBlue">
	<div class="GridWrapper">
		<div class="Grid DesktopOnly">
			<div class="LeftContentBlock">
				<h1>We help people Live healthier Live happier</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus, magnam.</p>
				<p class="ButtonElement"><a href="" class="Button"><span>Discover</span></a></p>
			</div>
		</div>

		<div class="Grid">
				<img src="assets/img/img/temp-img/banner-img.png">
		</div>

		<div class="Grid MobileOnly">
			<div class="LeftContentBlock">
				<h1>We help people Live healthier Live happier</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus, magnam.</p>
				<p class="ButtonElement"><a href="" class="Button"><span>Discover</span></a></p>
			</div>
		</div>

	</div>
</section>
